<?php

namespace App\EventListener;

use Symfony\Component\HttpKernel\Event\RequestEvent;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Brand;

class RequestListener
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function onKernelRequest(RequestEvent $event)
    {
        $brandName = str_replace('.brand.local', '', $event->getRequest()->getHost());

        if (!$brandName) {
            return;
        }

        $brandRepository = $this->em->getRepository(Brand::class);
        $brand = $brandRepository->findOneByName(ucfirst($brandName));

        if (!$brand) {
            return;
        }
        $conn = $this->em->getConnection();

        $conn->changeDatabase($brand->getHost(), 3306, $brand->getUser(), $brand->getPassword(), $brand->getDbname());
    }
}
